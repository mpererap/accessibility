package com.example.accessibilityexample

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val firstFragment =
            NavigationExampleFragment().apply { navigationLambda = ::navigateToFragment }

        supportFragmentManager.beginTransaction()
            .apply { add(R.id.fragmentContainer, firstFragment, NavigationExampleFragment.TAG) }
            .apply { commit() }
    }

    private fun navigateToFragment(fragment: Fragment, tag: String) {
        supportFragmentManager.beginTransaction()
            .apply { replace(R.id.fragmentContainer, fragment, tag) }
            .apply { addToBackStack(tag) }
            .apply { commit() }
    }
}
