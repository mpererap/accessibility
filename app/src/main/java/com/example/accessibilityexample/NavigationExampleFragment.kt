package com.example.accessibilityexample

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import kotlinx.android.synthetic.main.navigation_example_fragment.*

class NavigationExampleFragment : Fragment() {

    var navigationLambda: ((Fragment, String) -> Unit)? = null

    private val dynamicFragment: DynamicExampleFragment by lazy {
        DynamicExampleFragment().apply {
            navigationLambda = this@NavigationExampleFragment.navigationLambda
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.navigation_example_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        learnButton.setOnClickListener {
            navigationLambda?.invoke(dynamicFragment, DynamicExampleFragment.TAG)
        }

        AccessibilityHelper.setDescriptionClassAndActionToView(learnButton, "Botón para aprender", Button::class.java.name,
            AccessibilityNodeInfoCompat.AccessibilityActionCompat.ACTION_CLICK, "Pasar a la siguiente pantalla")
    }

    companion object {
        const val TAG = "NavigationExample"
    }
}