package com.example.accessibilityexample

import android.content.res.Resources
import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View

class DynamicItemDecoration : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect, view: View,
        parent: RecyclerView, state: RecyclerView.State
    ) {
        val position = parent.getChildAdapterPosition(view)
        val lastPosition = (parent.adapter?.itemCount?.takeIf { it > 0 }) ?: 0
        if (position in 1 until lastPosition) {
            outRect.left = toPx(ITEM_SEPARATION_DP)
        }
    }

    companion object {
        @JvmStatic
        fun toPx(dp: Int): Int = (dp * Resources.getSystem().displayMetrics.density).toInt()

        const val ITEM_SEPARATION_DP = 5
    }
}