package com.example.accessibilityexample

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup

class DynamicAdapter : RecyclerView.Adapter<DynamicViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): DynamicViewHolder =
        DynamicViewHolder(parent)

    override fun getItemCount(): Int = LIST_SIZE

    override fun onBindViewHolder(viewHolder: DynamicViewHolder, position: Int) {
        viewHolder.update(position)
    }

    companion object {
        const val LIST_SIZE = 12
    }
}