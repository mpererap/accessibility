package com.example.accessibilityexample

import android.content.Context
import android.support.v4.view.AccessibilityDelegateCompat
import android.support.v4.view.ViewCompat
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat
import android.view.View
import android.view.accessibility.AccessibilityManager
import android.widget.Button

/**
 * Helper for accessibility purposes.
 */
class AccessibilityHelper {

    companion object {

        /**
         * Indicates to the accessibility reader that the given [view] is a [className] even if it
         * is not.
         *
         * @param view View we wish to work on.
         * @param className Name of the class we wish the view to be.
         */
        @JvmStatic
        fun setClassToView(view: View?, className: CharSequence?): Unit? =
            view?.let {
                ViewCompat.setAccessibilityDelegate(it, object : AccessibilityDelegateCompat() {
                    override fun onInitializeAccessibilityNodeInfo(
                        host: View,
                        info: AccessibilityNodeInfoCompat
                    ) {
                        super.onInitializeAccessibilityNodeInfo(host, info)
                        info.className = className
                    }
                })
            }

        /**
         * Indicates to the accessibility reader that the given [view] is a [className] even if it
         * is not.
         *
         * @param view View we wish to work on.
         * @param className Name of the class we wish the view to be.
         */
        @JvmStatic
        fun setClassAndDescriptionToView(view: View?, className: CharSequence?, description: String): Unit? =
            view?.let {
                ViewCompat.setAccessibilityDelegate(it, object : AccessibilityDelegateCompat() {
                    override fun onInitializeAccessibilityNodeInfo(
                        host: View,
                        info: AccessibilityNodeInfoCompat
                    ) {
                        super.onInitializeAccessibilityNodeInfo(host, info)
                        info.className = className
                        info.contentDescription = description
                    }
                })
            }

        /**
         * Sets the class and the action to a given view.
         *
         * @param view View we wish to work on.
         * @param description Description for talkback.
         * @param className Name of the class the view has to use.
         * @param action Type of action that will trigger the [actionDescription]
         * @param actionDescription Description that will be announced when the [action] is realized
         */
        @JvmStatic
        fun setDescriptionClassAndActionToView(view: View, description: String, className: String, action: AccessibilityNodeInfoCompat.AccessibilityActionCompat, actionDescription: String): Unit? =
            view?.let {
                ViewCompat.setAccessibilityDelegate(it, object : AccessibilityDelegateCompat() {
                    override fun onInitializeAccessibilityNodeInfo(
                        host: View,
                        info: AccessibilityNodeInfoCompat
                    ) {
                        super.onInitializeAccessibilityNodeInfo(host, info)
                        info.className = className
                        info.addAction(
                            AccessibilityNodeInfoCompat.AccessibilityActionCompat(
                                action.id,
                                actionDescription
                            )
                        )
                        info.contentDescription = description
                    }
                })
            }

        /**
         * Indicates to the accessibility reader that the given [view] has the given [action] on click
         *
         * @param view View we wish to work on.
         * @param action Action to be triggered upon click
         */
        @JvmStatic
        fun setActionToView(view: View?, action: AccessibilityNodeInfoCompat.AccessibilityActionCompat, actionDescription: String?): Unit? =
            view?.let {
                ViewCompat.setAccessibilityDelegate(it, object : AccessibilityDelegateCompat() {
                    override fun onInitializeAccessibilityNodeInfo(
                        host: View,
                        info: AccessibilityNodeInfoCompat
                    ) {
                        super.onInitializeAccessibilityNodeInfo(host, info)
                        info.addAction(
                            AccessibilityNodeInfoCompat.AccessibilityActionCompat(
                                action.id,
                                actionDescription
                            )
                        )
                    }
                })
            }


        /**
         * Indicates to the accessibility reader that the given [view] has the
         *
         * @param view View we wish to work on.
         * @param description Description to be read by Talkback.
         * @param action Action to be read by Talkback (after description).
         */
        @JvmStatic
        fun setDescriptionAndActionToView(view: View?, description: String, action: String): Unit? =
            view?.let {
                ViewCompat.setAccessibilityDelegate(it, object : AccessibilityDelegateCompat() {
                    override fun onInitializeAccessibilityNodeInfo(
                        host: View,
                        info: AccessibilityNodeInfoCompat
                    ) {
                        super.onInitializeAccessibilityNodeInfo(host, info)
                        info.contentDescription = description
                        info.addAction(
                            AccessibilityNodeInfoCompat.AccessibilityActionCompat(
                                AccessibilityNodeInfoCompat.ACTION_CLICK,
                                action
                            )
                        )
                    }
                })
            }

        /**
         * Accessibility delegate designed for OpenChatButtonView.
         *
         * @param view  View we wish to work on.
         */
        @JvmStatic
        fun setLearnButtonAccessibilityDelegate(view: View?): Unit? =
            view?.let {
                ViewCompat.setAccessibilityDelegate(it, object : AccessibilityDelegateCompat() {
                    override fun onInitializeAccessibilityNodeInfo(
                        host: View,
                        info: AccessibilityNodeInfoCompat
                    ) {
                        super.onInitializeAccessibilityNodeInfo(host, info)
                        view.context?.let {
                            val resources = view.resources
                            info.contentDescription = resources.getString(R.string.learn)
                            info.addAction(
                                AccessibilityNodeInfoCompat.AccessibilityActionCompat(
                                    AccessibilityNodeInfoCompat.ACTION_CLICK,
                                    resources.getString(R.string.accessibility_to_go_to_next_example)
                                )
                            )
                        }
                        info.className = Button::class.java.name
                    }
                })
            }

        /**
         * Accessibility delegate that takes a view and label it as an accessibility heading.
         *
         * @param view  View we wish to work on.
         */
        @JvmStatic
        fun setViewAsHeading(view: View?): Unit? =
            view?.let {
                ViewCompat.setAccessibilityDelegate(it, object : AccessibilityDelegateCompat() {
                    override fun onInitializeAccessibilityNodeInfo(
                        host: View,
                        info: AccessibilityNodeInfoCompat
                    ) {
                        super.onInitializeAccessibilityNodeInfo(host, info)
                        info.collectionItemInfo?.let { collectionInfo ->
                            info.setCollectionItemInfo(
                                AccessibilityNodeInfoCompat.CollectionItemInfoCompat.obtain(
                                    collectionInfo.rowIndex,
                                    collectionInfo.rowSpan,
                                    collectionInfo.columnIndex,
                                    collectionInfo.columnSpan,
                                    true,
                                    collectionInfo.isSelected
                                )
                            )
                        } ?: info.setCollectionItemInfo(
                            AccessibilityNodeInfoCompat.CollectionItemInfoCompat.obtain(
                                0,
                                0,
                                0,
                                0,
                                true,
                                false
                            )
                        )
                    }
                })
            }

        /**
         * Determines from a context if some accessibility option is enabled.
         *
         * @param context   Context that contains the information needed to determine if accessibility
         *                  is active.
         * @return True is some form of accessibility is active. False otherwise.
         */
        @JvmStatic
        fun isAccessibilityEnabled(context: Context?): Boolean =
            context?.let {
                val am = context.getSystemService(Context.ACCESSIBILITY_SERVICE) as? AccessibilityManager
                val isAccessibilityEnabled = am?.isEnabled ?: false
                val isExploreByTouchEnabled = am?.isTouchExplorationEnabled ?: false
                isAccessibilityEnabled || isExploreByTouchEnabled
            } ?: false

        /**
         * Password was being read twice on TalkBack, one from the hint and another from the input
         * type. This function indicates that the view is not a password so only the hint is being
         * read.
         *
         * @param info Object with all the information that TalkBack needs to read the text out loud.
         */
        private fun ifPasswordDoNotReadPasswordTwice(info: AccessibilityNodeInfoCompat) {
            info.isPassword = false
        }

        /**
         * Adds the error description to the contentDescription. This way the user knows if there is
         * something wrong with his text.
         *
         * @param info  Object with all the information that TalkBack needs to read the text out loud.
         * @param error Description of what is wrong with the current text.
         */
        private fun addErrorToDescription(info: AccessibilityNodeInfoCompat, error: CharSequence) {
            info.text = StringBuilder().apply {
                info.text?.let {
                    this.append(info.text).append(".")
                }
            }.apply { this.append(error) }.toString()
        }
    }
}