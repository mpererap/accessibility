package com.example.accessibilityexample

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import kotlinx.android.synthetic.main.dynamic_holder.view.*

class DynamicViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(
        R.layout.dynamic_holder,
        parent,
        false
    )
) {

    fun update(position: Int) {
        itemView.dynamicHolderText.text = position.toString()
    }
}