package com.example.accessibilityexample

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.view.AccessibilityDelegateCompat
import android.support.v4.view.ViewCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.accessibility.AccessibilityEvent
import android.widget.TextView
import kotlinx.android.synthetic.main.dynamic_example_fragment.*
import kotlinx.android.synthetic.main.navigation_example_fragment.learnButton

class DynamicExampleFragment : Fragment() {

    var navigationLambda: ((Fragment, String) -> Unit)? = null

    private val groupingFragment: GroupingExampleFragment by lazy { GroupingExampleFragment() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.dynamic_example_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        learnButton.setOnClickListener {
            navigationLambda?.invoke(groupingFragment, GroupingExampleFragment.TAG)
        }

        recyclerView.adapter = DynamicAdapter()
        recyclerView.addItemDecoration(DynamicItemDecoration())

        AccessibilityHelper.setDescriptionAndActionToView(textWithStatus, "Estado desactivado", "activar")

        ViewCompat.setAccessibilityDelegate(dynamicSwitch, object: AccessibilityDelegateCompat() {
            override fun onInitializeAccessibilityEvent(host: View?, event: AccessibilityEvent?) {
                super.onInitializeAccessibilityEvent(host, event)
                // Capturamos el evento
                if (event?.eventType == AccessibilityEvent.TYPE_VIEW_ACCESSIBILITY_FOCUSED){
                    recyclerView.layoutManager?.scrollToPosition(0)
                }
            }
        })

        ViewCompat.setAccessibilityDelegate(dynamicCheckBox, object: AccessibilityDelegateCompat() {
            override fun onInitializeAccessibilityEvent(host: View?, event: AccessibilityEvent?) {
                super.onInitializeAccessibilityEvent(host, event)
                // Capturamos el evento
                if (event?.eventType == AccessibilityEvent.TYPE_VIEW_ACCESSIBILITY_FOCUSED){
                    recyclerView.layoutManager?.scrollToPosition(recyclerView.adapter!!.itemCount - 1)
                }
            }
        })

        val deactivatedColor: Int = context?.let { ContextCompat.getColor(it, android.R.color.holo_red_light) } ?: 0
        val activatedColor: Int = context?.let { ContextCompat.getColor(it, android.R.color.holo_green_light) } ?: 0

        textWithStatus.setOnClickListener {
            if ((it as TextView).currentTextColor == deactivatedColor){
                it.setTextColor(activatedColor)
                AccessibilityHelper.setDescriptionAndActionToView(it, "Estado activado", "desactivar")

            } else {
                it.setTextColor(deactivatedColor)
                AccessibilityHelper.setDescriptionAndActionToView(it, "Estado desactivado", "activar")

            }
        }
    }

    companion object {
        const val TAG = "DynamicExample"
    }
}